#include <FastLED.h>

#define LED_PIN 6
#define DOWN_PIN 2
#define UP_PIN 3
#define OK_PIN 4

#define CHIPSET NEOPIXEL
#define NUM_LEDS 36

#define BRIGHTNESS 64
#define FRAMES_PER_SECOND 60

CRGB leds[NUM_LEDS];

enum class ClicT { None = 0,
                   Down,
                   Up,
                   Ok,
                   OkLong };

class Clic {
public:
  Clic(uint16_t long_press_duration)
    : pressed{ 0 }, button{ ClicT::None }, ok_long_accu_{ 0 }, long_press_duration_(long_press_duration) {}

  ClicT getClic() {
    if (digitalRead(OK_PIN) == 0) {
      button = ClicT::Ok;
      pressed = 1;
      ok_long_accu_++;
    } else if (digitalRead(UP_PIN) == 0) {
      button = ClicT::Up;
      pressed = 1;
      ok_long_accu_ = 0;
    } else if (digitalRead(DOWN_PIN) == 0) {
      button = ClicT::Down;
      pressed = 1;
      ok_long_accu_ = 0;
    } else if (pressed) {
      pressed = 0;
      if (button == ClicT::Ok and ok_long_accu_ > long_press_duration_) {
        ok_long_accu_ = 0;
        return ClicT::OkLong;
      } else {
        ok_long_accu_ = 0;
        return button;
      }
    }
    return ClicT::None;
  }

private:
  bool pressed{ 0 };
  ClicT button{};
  uint32_t ok_long_accu_;
  uint16_t long_press_duration_;
};

static Clic clicky{ 200 };

void setup() {
  FastLED.addLeds<CHIPSET, LED_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(BRIGHTNESS);
  Serial.begin(9600);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
}
static uint16_t period_flame{ 5 };
static uint16_t counter_flame{ 0 };
static uint16_t period_boum_boum{ 150 };
static uint16_t counter_boum_boum{ 0 };

uint8_t updateFlameSpeed(ClicT button) {
  constexpr static uint8_t flameSpeeds[3] = { 8, 4, 2 };
  static uint8_t current_speed_index{ 1 };
  switch (button) {
    case ClicT::Up:
      current_speed_index = (current_speed_index + 1) % 3;
      break;
    case ClicT::Down:
      current_speed_index = (current_speed_index + 2) % 3;
      break;
  }
  period_flame = flameSpeeds[current_speed_index];
}

void updateBpm(ClicT button) {
  switch (button) {
    case ClicT::Up:
      period_boum_boum = qsub8(period_boum_boum, 2);
      break;
    case ClicT::Down:
      period_boum_boum = qadd8(period_boum_boum, 2);
      break;
  }
}

static bool bpmMode{ false };

void loop() {
  random16_add_entropy(random());
  ClicT button = clicky.getClic();

  if (button == ClicT::OkLong) {
    bpmMode = not bpmMode;
  } else if (button == ClicT::Ok) {
    counter_boum_boum = 0;
  }

  if (bpmMode) {
    updateBpm(button);
  } else {
    updateFlameSpeed(button);
  }

  if (counter_flame > period_flame) {
    counter_flame = 0;
    fire();      // run simulation frame
    FastLED.show();  // display this frame
  } else {
    FastLED.delay(1);
  }
  counter_flame++;
  if (bpmMode) {
    int a = getBrightness(period_boum_boum, 1, 1);
    FastLED.setBrightness(a);
  } else {
    FastLED.setBrightness(BRIGHTNESS);
  }
}



int getBrightness(uint16_t period, uint8_t divider_intensity, uint8_t divider_wave) {
  const char txt[10]{};
  uint8_t v{ 0 };
  static bool blink_smol{ 0 };
  enum class Blink { NoBlink,
                     Blink,
                     NoBlinkSmol,
                     BlinkSmol,
  };
  static Blink blink{ Blink::NoBlink };
  static bool blinkDone{ 0 };

  counter_boum_boum++;
  switch (blink) {
    case Blink::NoBlink:
      if (counter_boum_boum > period) {
        blink = Blink::Blink;
        counter_boum_boum = 0;
      }
      break;
    case Blink::Blink:
      if (blinkDone) {
        blinkDone = 0;
        blink = Blink::NoBlinkSmol;
      }
      break;
    case Blink::NoBlinkSmol:
      if (counter_boum_boum > period) {
        blink = Blink::BlinkSmol;
        counter_boum_boum = 0;
      }
      break;
    case Blink::BlinkSmol:
      if (blinkDone) {
        blinkDone = 0;
        blink = Blink::NoBlink;
      }
      break;
  }

  if ((blink == Blink::BlinkSmol) or (blink == Blink::Blink)) {
    v = sin8(counter_boum_boum << divider_wave);
    if ((v - 128) < 0) {
      v = 0;
      blinkDone = 1;
    } else if (blink == Blink::Blink) {
      v = (v - 128) >> divider_intensity;
    } else {
      v = (v - 128) >> (2 * divider_intensity);
    }
  } else {
    v = 0;
  }

  return qadd8(BRIGHTNESS, v);
}

// Inspired by Fire2012 by Mark Kriegsman, July 2012 - https://pastebin.com/xYEpxqgq
// Modified to separate simulation speed from flame movement speed, to be smoother
// Changed cooling to depend on altitude
// Changed heat diffusion to interpolate simulations to leds
#define SPARKING 128
#define SLOW_COEFF 4
#define SIM_LEDS NUM_LEDS* SLOW_COEFF
#define OFFSET_LED 3
void fire() {
  static byte heat[SIM_LEDS];

  // Step 1.  Cool down every cell a little
  for (int i = SLOW_COEFF; i < SIM_LEDS; i++) {
    heat[i] = qsub8(heat[i], ((i - SLOW_COEFF) >> SLOW_COEFF));
  }

  // Step 2.  Heat from each cell drifts 'up' and diffuses a little
  for (int k = SIM_LEDS - 3; k > 0; k--) {
    uint16_t accum = 0;
    uint8_t div_accum = 0;
    for (int l = 1; l <= SLOW_COEFF; l++) {
      div_accum += (SLOW_COEFF - l + 1);
      accum += (heat[k - l]) * (SLOW_COEFF - l + 1);
    }
    heat[k] = accum / (div_accum);
  }

  // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
  if (random8() < SPARKING) {
    for (int i = 0; i < (SLOW_COEFF + 1) / 2; i++) {
      heat[i] = qadd8(heat[i], random8(160, 255));
    }
  }

  // Step 4.  Map from heat cells to LED colors
  for (int j = 0; j < NUM_LEDS - OFFSET_LED; j++) {
    leds[j + OFFSET_LED] = HeatColor(heat[SLOW_COEFF * j]);
  }
  for (int j = 0; j < OFFSET_LED; j++) {
    leds[j] = HeatColor(0xff);
  }
}